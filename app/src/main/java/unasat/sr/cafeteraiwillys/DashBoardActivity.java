package unasat.sr.cafeteraiwillys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class DashBoardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        Intent dashBoardIntent = getIntent();
        String userTypeId = dashBoardIntent.getStringExtra("userType");
        System.out.println(userTypeId);
    }
}
