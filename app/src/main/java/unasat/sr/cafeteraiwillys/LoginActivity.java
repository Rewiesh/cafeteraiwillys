package unasat.sr.cafeteraiwillys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import unasat.sr.cafeteraiwillys.DAO.CafeteraiWillysDAO;
import unasat.sr.cafeteraiwillys.Entities.User;
import unasat.sr.cafeteraiwillys.Entities.UserType;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginBtn;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        registerBtn = (Button) findViewById(R.id.registerBtn);
    }

    public void authenticateUser (View view) {
        Boolean validCred = false;
        User user;
        CafeteraiWillysDAO cafeteraiWillysDAO = new CafeteraiWillysDAO(this);
        if (usernameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(),"Enter your credentials!",Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            user = cafeteraiWillysDAO.findOneRecordByUsername(usernameEditText.getText().toString());
        }

        if (user != null) {
            if (user.getUsername().equals(usernameEditText.getText().toString()) && user.getPassword().equals(passwordEditText.getText().toString())) {
                validCred = true;
                UserType userType = cafeteraiWillysDAO.findOneRecordByUserType(user.getUserTypeId());
                Intent dashBoardActivityIntent = new Intent(getApplicationContext(), DashBoardActivity.class);
                dashBoardActivityIntent.putExtra("userType", userType.getUserType());
                startActivity(dashBoardActivityIntent);
            }
        }
        else {
            Toast.makeText(getApplicationContext(),"Wrong Username/password",Toast.LENGTH_SHORT).show();
        }
    }

    public void registerUser (View view) {
        Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(registerIntent);
    }
}
