package unasat.sr.cafeteraiwillys.DAO;

public class FoodItemsTable {

    public final static String tableName = "food_items";
    public final static String id = "id";
    public final static String name = "name";
    public final static String price = "price";
    public final static String itemCategory = "item_category";

    public final static String create_foodItemsTable_script = "create table food_items (        id              INTEGER PRIMARY KEY AUTOINCREMENT" +
                                                                                         "  ,   name            String      not null    unique" +
                                                                                         "  ,   price           INTEGER     not null" +
                                                                                         ");";
}
