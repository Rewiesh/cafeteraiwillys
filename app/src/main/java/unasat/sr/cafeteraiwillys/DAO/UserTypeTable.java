package unasat.sr.cafeteraiwillys.DAO;

public class UserTypeTable {

    public static String tableName = "user_types";
    public static String id = "id";
    public static String userType = "user_type";

    public static String create_table_script = "create table user_types (       id                 INTEGER PRIMARY KEY AUTOINCREMENT" +
                                                                            ",  user_type          String unique not null" +
                                                                            ");";
    }
