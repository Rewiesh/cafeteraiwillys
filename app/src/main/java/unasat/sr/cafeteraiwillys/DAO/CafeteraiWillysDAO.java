package unasat.sr.cafeteraiwillys.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import unasat.sr.cafeteraiwillys.Entities.FoodItem;
import unasat.sr.cafeteraiwillys.Entities.User;
import unasat.sr.cafeteraiwillys.Entities.UserType;

public class CafeteraiWillysDAO extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "cafeteraiWillys.db";
    public static int DATABASE_VERSION = 2;

    public CafeteraiWillysDAO(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        insertDefaultUser();
        insertDefaultFoodItem();
    }

    @Override
    public void onCreate( SQLiteDatabase db ) {
        db.execSQL(UserTypeTable.create_table_script);
        db.execSQL(UsersTable.create_users_table_script);
        db.execSQL(FoodItemsTable.create_foodItemsTable_script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertDefaultUser (){
        User checkIfUserExits = findOneRecordByUsername("manager");
        if (checkIfUserExits != null) {
            return;
        }

        List<UserType> userTypesList = new ArrayList<>();
        UserType manager = new UserType(null, "Manager");
        UserType gebruikers = new UserType(null, "Gebruiker");
        userTypesList.add(manager);
        userTypesList.add(gebruikers);

        insertUserType(userTypesList);

        UserType userType = findOneRecordByUserType("Manager");
        User user = new User(null, "manager", "manager@gmail.com","123", "Rewiesh", "Ramcharan", "02/11/1998",  null, null, userType.getId());
        insertUser(user);
    }

    public void insertDefaultFoodItem() {
        FoodItem checkIfFoodItemExists = findOneRecordByFoodItem("Bami");
        if (checkIfFoodItemExists != null) {
            return;
        }

        FoodItem newFoodItem = new FoodItem(null, "Bami", 20.00);
        insertFoodItem(newFoodItem);
    }


    public void insertUserType ( String userTypeVal ) {

        UserType userType = findOneRecordByUserType(userTypeVal);

        if (userType != null) {
            return;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserTypeTable.userType, userTypeVal);
        insertOneRecord(UserTypeTable.tableName, contentValues);
    }

    public void insertUserType ( List<UserType> userTypesList ) {
        List<ContentValues> contentValuesList = new ArrayList<>();

        for (int i =0; i != userTypesList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            UserType userType = findOneRecordByUserType(userTypesList.get(i).getUserType());
            if (userType != null) {
                continue;
            }
            else {
                contentValues.put(UserTypeTable.userType, userTypesList.get(i).getUserType());
                contentValuesList.add(contentValues);
            }
        }
        insertMultipleRecord(UserTypeTable.tableName, contentValuesList);
    }



    public void insertUser (User newUser) {
        User user = findOneRecordByUsername(newUser.getUsername());
        if (user != null) {
            return;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(UsersTable.username , newUser.getUsername());
        contentValues.put(UsersTable.email, newUser.getEmail());
        contentValues.put(UsersTable.password , newUser.getPassword());
        contentValues.put(UsersTable.first_name , newUser.getFirstname());
        contentValues.put(UsersTable.last_name , newUser.getLastname());
        contentValues.put(UsersTable.date_of_birth , newUser.getDateOfBirth());
        contentValues.put(UsersTable.created_by , newUser.getCreatedBy());
        contentValues.put(UsersTable.created_date , newUser.getCreatedDate());
        contentValues.put(UsersTable.user_type_id , newUser.getUserTypeId());

        insertOneRecord(UsersTable.tableName, contentValues);
    }

    public void insertFoodItem (FoodItem foodItem) {
        FoodItem checkIfFoodItemExits = findOneRecordByFoodItem(foodItem.getName());

        if (checkIfFoodItemExits != null) {
            return;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(FoodItemsTable.name, foodItem.getName());
        contentValues.put(FoodItemsTable.price, foodItem.getPrice());

        insertOneRecord(FoodItemsTable.tableName, contentValues);
    }


    public User findOneRecordByUsername (String username) {
        User user = null;
        SQLiteDatabase db = getReadableDatabase();
        String sql = String.format("select * from %s where username = '%s'", UsersTable.tableName, username);
        Cursor cursor = db.rawQuery(sql, null);
        //System.out.println(cursor.getCount());
        if (cursor.getCount() == 0) {
            user = null;
        }
        else if (cursor.moveToFirst()) {
            user = new User(cursor.getLong(0)        //id
                    ,cursor.getString(1)     //username
                    ,cursor.getString(2)     //email
                    ,cursor.getString (3)    //password
                    ,cursor.getString(4)     //firstname
                    ,cursor.getString(5)     //lastname );
                    ,cursor.getString(6)     //date_of_birth );
                    ,cursor.getString(7)       //user_type_id);
                    ,cursor.getString(8)     //created_date );
                    ,cursor.getLong(9)     //created_by
            );

        }
        db.close();
        return user;
    }

    public UserType findOneRecordByUserType (String userType) {
        UserType userType1 = null;
        SQLiteDatabase db = getReadableDatabase();

        String query = String.format("select * from %s where %s = ?", UserTypeTable.tableName, UserTypeTable.userType );
        Cursor cursor = db.rawQuery(query, new String[] {userType});

        if (cursor.moveToFirst()) {
            userType1 = new UserType(cursor.getLong(0), cursor.getString(1));
        }
        db.close();
        return userType1;
    }

    public UserType findOneRecordByUserType (Long userTypeId) {
        UserType userType1 = null;
        SQLiteDatabase db = getReadableDatabase();

        String query = String.format("select * from %s where %s = '%s'", UserTypeTable.tableName, UserTypeTable.id, userTypeId);
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            userType1 = new UserType(cursor.getLong(0), cursor.getString(1));
        }
        db.close();
        return userType1;
    }

    public FoodItem findOneRecordByFoodItem (String foodItemName) {
        FoodItem foodItem = null;
        SQLiteDatabase db = getReadableDatabase();

        String query = String.format("select * from %s where %s = '%s'", FoodItemsTable.tableName,FoodItemsTable.name, foodItemName);
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            foodItem = new FoodItem(cursor.getLong(0), cursor.getString(1), cursor.getDouble(2));
        }
        return foodItem;
    }

    public Long insertOneRecord (String tableName, ContentValues contentValues) {
        SQLiteDatabase db = getWritableDatabase();
        long rowId = db.insert(tableName, null, contentValues);
        db.close();
        return rowId;

    }

    public Boolean insertMultipleRecord (String tableName, List<ContentValues> contentValuesList) {
        SQLiteDatabase db = getWritableDatabase();
        long countOnSucces = 0;
        long rowId = 0;
        for (ContentValues contentValues : contentValuesList) {
            rowId = db.insert(tableName, null, contentValues);
            countOnSucces = (rowId == 1 ? countOnSucces++ : countOnSucces);
        }
        boolean isSuccess = (countOnSucces > 0 && contentValuesList.size() == countOnSucces);
        db.close();
        return isSuccess;
    }
}
