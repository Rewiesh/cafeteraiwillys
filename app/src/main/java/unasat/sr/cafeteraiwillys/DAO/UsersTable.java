package unasat.sr.cafeteraiwillys.DAO;


public class UsersTable {

    public static String tableName = "users";
    public static String id;
    public static final String username = "username";
    public static final String email = "emailadres";
    public static final String password = "password";
    public static final String first_name = "first_name";
    public static final String last_name = "last_name";
    public static final String date_of_birth = "date_of_birth";
    public static final String user_type_id = "user_type_id";
    public static final String created_by = "created_by";
    public static final String created_date = "created_date";
    public static final String created_datea = "created_date";



    public static final String usersTableName = "users";
    public static final String create_users_table_script = "create table users (        id                 INTEGER PRIMARY KEY AUTOINCREMENT" +
                                                                                    ",  username           String unique not null" +
                                                                                    ",  emailadres         String not null unique" +
                                                                                    ",  password           String not null" +
                                                                                    ",  first_name         String not null" +
                                                                                    ",  last_name          String not null" +
                                                                                    ",  date_of_birth      String" +
                                                                                    ",  created_by         String" +
                                                                                    ",  created_date       String" +
                                                                                    ",  user_type_id       INTEGER not null " +
                                                                                    ",  foreign key (user_type_id) references user_types (id)" +
                                                                                    ");";

}

