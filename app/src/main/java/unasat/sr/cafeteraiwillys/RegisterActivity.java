package unasat.sr.cafeteraiwillys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import unasat.sr.cafeteraiwillys.DAO.CafeteraiWillysDAO;
import unasat.sr.cafeteraiwillys.Entities.User;
import unasat.sr.cafeteraiwillys.Entities.UserType;

public class RegisterActivity extends AppCompatActivity {

    private DatePickerDialog datePickerDialog;
    private EditText usernameEditText;
    private EditText emailEditText;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText passwordEditText;
    private EditText reEnterPasswordEditText;
    private EditText dateOfBirthEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText =  (EditText) findViewById(R.id.lastNameEditText);
        usernameEditText =  (EditText) findViewById(R.id.usernameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        reEnterPasswordEditText = (EditText) findViewById(R.id.reEnterPasswordEditText);
        dateOfBirthEditText = (EditText) findViewById(R.id.dateOfBirthEditText);
        dateOfBirthEditText.setInputType(InputType.TYPE_NULL);

        dateOfBirthEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                datePickerDialog = new DatePickerDialog(RegisterActivity.this, R.style.CustomDatePickerDialogTheme,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                dateOfBirthEditText.setText(dayOfMonth + "/" + (month) + "/" + year);
                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
    }

    public void registerUser (View view) {

        if (firstNameEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide your first name.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (lastNameEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide your last name.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (dateOfBirthEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide your date of birth.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (usernameEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide a username.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (emailEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide a e-mailadres.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (passwordEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Please provide a password.",Toast.LENGTH_SHORT).show();
            return;
        }
        else if (reEnterPasswordEditText.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Re-enter your password.",Toast.LENGTH_SHORT).show();
            return;
        }

        CafeteraiWillysDAO cafeteraiWillysDAO = new CafeteraiWillysDAO(this);
        UserType userType = cafeteraiWillysDAO.findOneRecordByUserType("Gebruiker");
        User user = cafeteraiWillysDAO.findOneRecordByUsername(usernameEditText.getText().toString());
        if (user != null){
            Toast.makeText(getApplicationContext(),"User already exists!!",Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            User newUser = new User(null
                                    , usernameEditText.getText().toString()
                                    , emailEditText.getText().toString()
                                    , passwordEditText.getText().toString()
                                    , firstNameEditText.getText().toString()
                                    , lastNameEditText.getText().toString()
                                    , dateOfBirthEditText.getText().toString()
                                    , null
                                    , null
                                    , userType.getId());

            cafeteraiWillysDAO.insertUser(newUser);
        }
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }
}
