package unasat.sr.cafeteraiwillys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import unasat.sr.cafeteraiwillys.DAO.CafeteraiWillysDAO;
import unasat.sr.cafeteraiwillys.Entities.FoodItem;
import unasat.sr.cafeteraiwillys.Entities.User;
import unasat.sr.cafeteraiwillys.Entities.UserType;

public class MainActivity extends AppCompatActivity {

    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CafeteraiWillysDAO cafeteraiWillysDAO = new CafeteraiWillysDAO(this);
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);


//        FoodItem foodItem = cafeteraiWillysDAO.findOneRecordByFoodItem("Bami");
//        UserType userType1 =  cafeteraiWillysDAO.findOneRecordByUserType("Manager");
//
//
//        UserType userType12 =  cafeteraiWillysDAO.findOneRecordByUserType("Gebruikers");
//        User user = cafeteraiWillysDAO.findOneRecordByUsername("manager");
//
//
//
//
//        System.out.println(user.toString());
//
//        System.out.println(foodItem.toString());
//
//        System.out.println(userType1.toString());
//
//        System.out.println(userType12.toString());
    }
}
